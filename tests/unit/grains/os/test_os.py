from dict_tools import data
import io
import pytest
import mock


PKG_DATA = """
          Name: system/kernel
       Summary: Core Kernel
   Description: Core operating system kernel, device drivers and other modules.
      Category: System/Core
         State: Installed
     Publisher: solaris
       Version: 11.4
        Branch: 11.4.0.0.1.15.0
Packaging Date: Fri Aug 17 00:31:07 2018
          Size: 35.05 MB
          FMRI: pkg://solaris/system/kernel@11.4-11.4.0.0.1.15.0:test_build
"""

PRTCONF_DATA = """
System Configuration:  Oracle Corporation  i86pc
Memory size: 8192 Megabytes
System Peripherals (Software Nodes):
"""

RELEASE_DATA = """
                             Oracle Solaris 11.4 X86
  Copyright (c) 1983, 2018, Oracle and/or its affiliates.  All rights reserved.
                            Assembled 16 August 2018
"""

RELEASE_OMNIOS_DATA = """
  OmniOS v11 r151012
  Copyright 2014 OmniTI Computer Consulting, Inc. All rights reserved.
  Use is subject to license terms.
"""

RELEASE_OPENINDIANA_DATA = """
             OpenIndiana Development oi_151.1.9 X86 (powered by illumos)
        Copyright 2011 Oracle and/or its affiliates. All rights reserved.
                        Use is subject to license terms.
                           Assembled 17 January 2014
"""


@pytest.mark.asyncio
async def test_load_osbuild(mock_hub, hub):
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": PKG_DATA})

    mock_hub.grains.solaris.os.os.load_osbuild = hub.grains.solaris.os.os.load_osbuild

    await mock_hub.grains.solaris.os.os.load_osbuild()

    assert mock_hub.grains.GRAINS.osbuild == "test_build"


@pytest.mark.asyncio
async def test_load_manufacturer(mock_hub, hub):
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": PRTCONF_DATA})

    mock_hub.grains.solaris.os.os.load_manufacturer = (
        hub.grains.solaris.os.os.load_manufacturer
    )

    await mock_hub.grains.solaris.os.os.load_manufacturer()

    assert mock_hub.grains.GRAINS.osmanufacturer == "Oracle Corporation"


@pytest.mark.asyncio
async def test_load_osinfo_smartos(mock_hub, hub):
    mock_hub.grains.GRAINS.kernel = "SunOS"
    mock_hub.grains.GRAINS.kernelversion = "joyent_20170706T001501Z"
    mock_hub.grains.GRAINS.smartos = True

    with mock.patch("os.path.exists", return_value=True):
        with mock.patch(
            "aiofiles.threadpool.sync_open", return_value=io.StringIO(""),
        ):
            mock_hub.grains.solaris.os.os.load_osinfo = (
                hub.grains.solaris.os.os.load_osinfo
            )
            await mock_hub.grains.solaris.os.os.load_osinfo()

    assert mock_hub.grains.GRAINS.os == "SmartOS"
    assert mock_hub.grains.GRAINS.oscodename == "unknown"
    assert mock_hub.grains.GRAINS.osfinger == "SmartOS-2017"
    assert mock_hub.grains.GRAINS.osfullname == "SmartOS-2017.7.6"
    assert mock_hub.grains.GRAINS.osmajorrelease == 2017
    assert mock_hub.grains.GRAINS.osrelease == "2017.7.6"
    assert mock_hub.grains.GRAINS.osrelease_info == (2017, 7, 6)


@pytest.mark.asyncio
async def test_load_osinfo_solaris(mock_hub, hub):
    mock_hub.grains.GRAINS.kernel = "SunOS"
    mock_hub.grains.GRAINS.kernelversion = "11.4.0.15.0"
    mock_hub.grains.GRAINS.smartos = False

    with mock.patch("os.path.exists", return_value=True):
        with mock.patch(
            "aiofiles.threadpool.sync_open", return_value=io.StringIO(RELEASE_DATA),
        ):
            mock_hub.grains.solaris.os.os.load_osinfo = (
                hub.grains.solaris.os.os.load_osinfo
            )
            await mock_hub.grains.solaris.os.os.load_osinfo()

    assert mock_hub.grains.GRAINS.os == "Oracle Solaris"
    assert mock_hub.grains.GRAINS.oscodename == "Nevada"
    assert mock_hub.grains.GRAINS.osfinger == "Oracle Solaris-11"
    assert mock_hub.grains.GRAINS.osfullname == "Oracle Solaris-11.4.0"
    assert mock_hub.grains.GRAINS.osmajorrelease == 11
    assert mock_hub.grains.GRAINS.osrelease == "11.4.0"
    assert mock_hub.grains.GRAINS.osrelease_info == (11, 4, 0)


@pytest.mark.asyncio
async def test_load_osinfo_omnios(mock_hub, hub):
    mock_hub.grains.GRAINS.kernel = "SunOS"
    mock_hub.grains.GRAINS.kernelversion = "11.4.0.15.0"
    mock_hub.grains.GRAINS.smartos = False

    with mock.patch("os.path.exists", return_value=True):
        with mock.patch(
            "aiofiles.threadpool.sync_open",
            return_value=io.StringIO(RELEASE_OMNIOS_DATA),
        ):
            mock_hub.grains.solaris.os.os.load_osinfo = (
                hub.grains.solaris.os.os.load_osinfo
            )
            await mock_hub.grains.solaris.os.os.load_osinfo()

    assert mock_hub.grains.GRAINS.os == "OmniOS"
    assert mock_hub.grains.GRAINS.oscodename == "unknown"
    assert mock_hub.grains.GRAINS.osfinger == "OmniOS-11"
    assert mock_hub.grains.GRAINS.osfullname == "OmniOS-11.151012"
    assert mock_hub.grains.GRAINS.osmajorrelease == 11
    assert mock_hub.grains.GRAINS.osrelease == "11.151012"
    assert mock_hub.grains.GRAINS.osrelease_info == (11, 151012)


@pytest.mark.asyncio
async def test_load_osinfo_openindiana(mock_hub, hub):
    mock_hub.grains.GRAINS.kernel = "SunOS"
    mock_hub.grains.GRAINS.kernelversion = "11.4.0.15.0"
    mock_hub.grains.GRAINS.smartos = False

    with mock.patch("os.path.exists", return_value=True):
        with mock.patch(
            "aiofiles.threadpool.sync_open",
            return_value=io.StringIO(RELEASE_OPENINDIANA_DATA),
        ):
            mock_hub.grains.solaris.os.os.load_osinfo = (
                hub.grains.solaris.os.os.load_osinfo
            )
            await mock_hub.grains.solaris.os.os.load_osinfo()

    assert mock_hub.grains.GRAINS.os == "Solaris"
    assert mock_hub.grains.GRAINS.oscodename == "unknown"
    assert mock_hub.grains.GRAINS.osfinger == "Solaris"
    assert mock_hub.grains.GRAINS.osfullname == "Solaris"
    assert mock_hub.grains.GRAINS.osmajorrelease == 0
    assert mock_hub.grains.GRAINS.osrelease == "0"
    assert mock_hub.grains.GRAINS.osrelease_info == (0,)
