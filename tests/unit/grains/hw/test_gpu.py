import pytest


@pytest.mark.asyncio
@pytest.mark.skip("Core function not implemented")
async def test_load_gpu(mock_hub, hub):
    mock_hub.exec.cmd.run.return_value = "TODO gpu_data cmd output"

    mock_hub.grains.solaris.hw.gpu.load_num_gpus = (
        hub.grains.solaris.hw.gpu.load_num_gpus
    )

    await mock_hub.grains.solaris.hw.gpu.load_num_gpus()

    assert mock_hub.grains.GRAINS.num_gpus == 99
    assert mock_hub.grains.GRAINS.gpus._dict() == {}
