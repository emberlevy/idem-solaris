from dict_tools import data
import pytest

IOSTAT_DATA = """
                  extended device statistics
device       r/s    w/s   kr/s   kw/s wait actv  svc_t  %w  %b
fd0          0.0    0.0    0.0    0.0  0.0  0.0    0.0   0   0
sd0          0.0    0.0    0.4    0.4  0.0  0.0   49.5   0   0
sd6          0.0    0.0    0.0    0.0  0.0  0.0    0.0   0   0
nfs1         0.0    0.0    0.0    0.0  0.0  0.0    0.0   0   0
nfs49        0.0    0.0    0.0    0.0  0.0  0.0   15.1   0   0
nfs53        0.0    0.0    0.4    0.0  0.0  0.0   24.5   0   0
nfs54        0.0    0.0    0.0    0.0  0.0  0.0    6.3   0   0
nfs55        0.0    0.0    0.0    0.0  0.0  0.0    4.9   0   0
"""


@pytest.mark.asyncio
async def test_load_disks(mock_hub, hub):
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": IOSTAT_DATA})

    mock_hub.grains.solaris.hw.slices.load_disks = (
        hub.grains.solaris.hw.slices.load_disks
    )

    await mock_hub.grains.solaris.hw.slices.load_disks()
    assert mock_hub.grains.GRAINS.disks == (
        "device",
        "fd0",
        "nfs1",
        "nfs49",
        "nfs53",
        "nfs54",
        "nfs55",
        "sd0",
        "sd6",
    )
    # TODO Haven't found a way to detect SSDs
    # assert mock_hub.grains.GRAINS.SSDs == ()
