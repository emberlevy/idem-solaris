from dict_tools import data
import pytest
import mock

ISAINFO_CPU_FLAG_DATA = """
64-bit amd64 applications
        prfchw rdrand avx xsave pclmulqdq aes movbe sse4_2 sse4_1
        ssse3 amd_lzcnt popcnt amd_sse4a tscp ahf cx16 sse3 sse2 sse
        fxsr amd_mmx mmx cmov amd_sysc cx8 tsc fpu svm
"""

KSTAT_DATA = """
cpu_info:0:cpu_info0:brand      test_model
cpu_info:1:cpu_info1:brand      test_model
cpu_info:2:cpu_info2:brand      test_model
cpu_info:3:cpu_info3:brand      test_model
cpu_info:4:cpu_info4:brand      test_model
cpu_info:5:cpu_info5:brand      test_model
cpu_info:6:cpu_info6:brand      test_model
cpu_info:7:cpu_info7:brand      test_model
cpu_info:8:cpu_info7:brand      test_model
cpu_info:9:cpu_info7:brand      test_model
"""

PSRINFO_DATA = """
0       on-line   since 04/20/2020 13:07:47
1       on-line   since 04/20/2020 13:07:47
2       on-line   since 04/20/2020 13:07:47
3       on-line   since 04/20/2020 13:07:47
4       on-line   since 04/20/2020 13:07:47
5       on-line   since 04/20/2020 13:07:47
6       on-line   since 04/20/2020 13:07:47
7       on-line   since 04/20/2020 13:07:47
8       on-line   since 04/20/2020 13:07:47
9       on-line   since 04/20/2020 13:07:47
"""


@pytest.mark.asyncio
async def test_load_cpu_arch(mock_hub, hub):
    mock_hub.grains.GRAINS.osarch = "testarch"

    with mock.patch("shutil.which", return_value=False):
        mock_hub.grains.solaris.hw.cpu.load_cpu_arch = (
            hub.grains.solaris.hw.cpu.load_cpu_arch
        )
        await mock_hub.grains.solaris.hw.cpu.load_cpu_arch()

    assert mock_hub.grains.GRAINS.cpuarch == "testarch"


@pytest.mark.asyncio
async def test_load_cpu_arch_isainfo(mock_hub, hub):
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": "testarch"})
    with mock.patch("shutil.which", return_value=True):
        mock_hub.grains.solaris.hw.cpu.load_cpu_arch = (
            hub.grains.solaris.hw.cpu.load_cpu_arch
        )
        await mock_hub.grains.solaris.hw.cpu.load_cpu_arch()

    assert mock_hub.grains.GRAINS.cpuarch == "testarch"


@pytest.mark.asyncio
async def test_load_cpu_flags(mock_hub, hub):
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict(
        {"stdout": ISAINFO_CPU_FLAG_DATA}
    )
    with mock.patch("shutil.which", return_value=True):
        mock_hub.grains.solaris.hw.cpu.load_cpu_flags = (
            hub.grains.solaris.hw.cpu.load_cpu_flags
        )
        await mock_hub.grains.solaris.hw.cpu.load_cpu_flags()

    assert mock_hub.grains.GRAINS.cpu_flags == (
        "aes",
        "ahf",
        "amd_lzcnt",
        "amd_mmx",
        "amd_sse4a",
        "amd_sysc",
        "avx",
        "cmov",
        "cx16",
        "cx8",
        "fpu",
        "fxsr",
        "mmx",
        "movbe",
        "pclmulqdq",
        "popcnt",
        "prfchw",
        "rdrand",
        "sse",
        "sse2",
        "sse3",
        "sse4_1",
        "sse4_2",
        "ssse3",
        "svm",
        "tsc",
        "tscp",
        "xsave",
    )
    assert mock_hub.grains.GRAINS.hardware_virtualization is True


@pytest.mark.asyncio
async def test_load_cpu_model(mock_hub, hub):
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": KSTAT_DATA})

    with mock.patch("shutil.which", return_value=True):
        mock_hub.grains.solaris.hw.cpu.load_cpu_model = (
            hub.grains.solaris.hw.cpu.load_cpu_model
        )
        await mock_hub.grains.solaris.hw.cpu.load_cpu_model()

    assert mock_hub.grains.GRAINS.cpu_model == "test_model"


@pytest.mark.asyncio
async def test_load_num_cpus(mock_hub, hub):
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": PSRINFO_DATA})

    with mock.patch("os.path.exists", return_value=True):
        mock_hub.grains.solaris.hw.cpu.load_num_cpus = (
            hub.grains.solaris.hw.cpu.load_num_cpus
        )
        await mock_hub.grains.solaris.hw.cpu.load_num_cpus()

    assert mock_hub.grains.GRAINS.num_cpus == 10
