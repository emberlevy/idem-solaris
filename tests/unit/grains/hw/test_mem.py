from dict_tools import data
import pytest
import mock

PRTCONF_DATA = """
System Configuration:  Oracle Corporation  i86pc
Memory size: 9999999999 Megabytes
System Peripherals (Software Nodes):

i86pc
    scsi_vhci, instance #0
    pci, instance #0
        pci8086,1237 (driver not attached)
        isa, instance #0
            i8042, instance #0
                keyboard, instance #0
                mouse, instance #0
            pit_beep, instance #0
        pci-ide, instance #0
            ide, instance #0
                cmdk, instance #0
            ide, instance #1 (driver not attached)
        display, instance #0
        pci8086,1e, instance #0
        pci80ee,cafe, instance #0
        pci8086,7113 (driver not attached)
    fw, instance #0
        cpu, instance #0 (driver not attached)
        cpu, instance #1 (driver not attached)
        cpu, instance #2 (driver not attached)
        cpu, instance #3 (driver not attached)
        cpu, instance #4 (driver not attached)
        cpu, instance #5 (driver not attached)
        cpu, instance #6 (driver not attached)
        cpu, instance #7 (driver not attached)
        sb, instance #1
    used-resources (driver not attached)
    fcoe, instance #0
    iscsi, instance #0
    agpgart, instance #0 (driver not attached)
    options, instance #0
    pseudo, instance #0
    vga_arbiter, instance #0 (driver not attached)
    xsvc, instance #0
"""


@pytest.mark.asyncio
async def test_load_meminfo(mock_hub, hub):
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": PRTCONF_DATA})

    with mock.patch("os.path.exists", return_value=True):
        mock_hub.grains.solaris.hw.mem.load_meminfo = (
            hub.grains.solaris.hw.mem.load_meminfo
        )
        await mock_hub.grains.solaris.hw.mem.load_meminfo()

    assert mock_hub.grains.GRAINS.mem_total == 9999999999


@pytest.mark.asyncio
async def test_load_swap(mock_hub, hub):
    SWAP_DATA = "total: 999999k bytes allocated + 999999k reserved = 1999998k used, 9999999k available"
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": SWAP_DATA})

    with mock.patch("shutil.which", return_value=True):
        mock_hub.grains.solaris.hw.mem.load_swap = hub.grains.solaris.hw.mem.load_swap
        await mock_hub.grains.solaris.hw.mem.load_swap()

    assert mock_hub.grains.GRAINS.swap_total == 11718
